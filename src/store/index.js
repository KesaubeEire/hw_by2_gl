import Vue from 'vue';
import Vuex from 'vuex';

import axios from 'axios';

Vue.use(Vuex);

function state_to_localStorage() {}

export default new Vuex.Store({
  state: {
    // 选择的标签
    myChannel: [],
    // 全部标签
    allChannel: [],
    // 当前标签
    nowChannel: null,

    // 全部文章
    allArticle: []
  },
  mutations: {
    // 初始化获取标签
    GETC(state, payload) {
      if (localStorage.getItem('vuex_news_tags_boolean')) {
        // localStorage 存在就读取
        state.myChannel = localStorage.getItem('vuex_news_state_my').split(',');
        state.allChannel = localStorage.getItem('vuex_news_state_all').split(',');
      } else {
        // localStorage 不存在就向服务器拉取
        state.allChannel = payload;
        state.myChannel = state.allChannel.splice(0, 4);

        // 向 localStorage 存储
        localStorage.setItem('vuex_news_tags_boolean', true);
        if (state.myChannel.length != 0) localStorage.setItem('vuex_news_state_my', state.myChannel);
        localStorage.setItem('vuex_news_state_all', state.allChannel);
      }
    },
    // 删除标签
    DELC(state, payload) {
      let item = state.myChannel.splice(payload, 1);
      console.log(item);
      state.allChannel.push(...item);

      // 向 localStorage 存储
      if (state.myChannel.length != 0) localStorage.setItem('vuex_news_state_my', state.myChannel);
      localStorage.setItem('vuex_news_state_all', state.allChannel);
    },
    // 添加标签
    ADD(state, payload) {
      let item = state.allChannel.splice(payload, 1);
      state.myChannel.push(...item);

      // 向 localStorage 存储
      if (state.myChannel.length != 0) localStorage.setItem('vuex_news_state_my', state.myChannel);
      localStorage.setItem('vuex_news_state_all', state.allChannel);
    },

    // 设置当前标签
    SET(state, payload) {
      state.nowChannel = state.myChannel[payload];
      console.log('当前 Channel:\t' + state.nowChannel);
    },

    // 初始化获取文章
    GETA(state, payload) {
      state.allArticle = payload;
    }
  },
  actions: {
    // 获取所有 Nav
    async getNav() {
      let {
        data: { result }
      } = await axios.get('/hw_vuex/json/nav.json');

      console.log('-------VUEX-geNav');
      console.log(result);
      this.commit('GETC', result);
    },
    // 获取所有 News
    async getNews() {
      let {
        data: {
          result: { list }
        }
      } = await axios.get('/hw_vuex/json/news.json');

      console.log('-------VUEX-getNews');
      console.log(list);
      this.commit('GETA', list);
    }
  },
  modules: {}
});
