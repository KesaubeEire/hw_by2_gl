import axios from 'axios';

let instance = axios.create({
  baseURL: 'https://lianghj.top:8888/api/private/v1/',
  timeout: 3000,
  headers: {
    Authorization: sessionStorage.token
  }
});

// 添加请求拦截器
instance.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    // 存在 token 的情况下将 token 配置过去
    if (sessionStorage.token) {
      config.headers.Authorization = sessionStorage.token;
    }
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器 -> 直接返回 data
instance.interceptors.response.use(
  function (response) {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么
    // console.log(response);
    return response.data;
  },
  function (error) {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    return Promise.reject(error);
  }
);

// 封装 Promise
export function httpConfig(url, method, data) {
  return new Promise((resolve, reject) => {
    instance({
      url: url,
      method: method,
      data: data,
      params: data
    })
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
}
