import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
  },
  {
    path: '/about',
    name: 'about',
    component: () => import(/* webpackChunkName: "About" */ '../views/About.vue')
  },
  // 纯纯的 axios 页面
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "loginView" */ '../views/LoginView.vue')
  },
  // news_axios 作业页面
  {
    path: '/news_a',
    name: 'news_a',
    component: () => import(/* webpackChunkName: "news_a" */ '../views/Page/Home.vue')
  },
  {
    path: '/detail_a/:id',
    name: 'detail_a',
    component: () => import(/* webpackChunkName: "detail_a" */ '../views/Page/Detail.vue')
  },
  // news_vuex 作业页面
  {
    path: '/news_v',
    name: 'news_v',
    component: () => import(/* webpackChunkName: "news_v" */ '../views/News_Vuex/index.vue')
  },
  {
    path: '/channelManage',
    name: 'channelManage',
    component: () => import(/* webpackChunkName: "channelManage" */ '../views/News_Vuex/channelManage.vue')
  },
  {
    path: '/jd_mobile',
    name: 'jd_mobile',
    component: () => import(/* webpackChunkName: "jd_mobile" */ '../views/JD_Mobile/Home.vue')
  },
  {
    path: '/jd_classify',
    name: 'jd_classify',
    component: () => import(/* webpackChunkName: "jd_classify" */ '../views/JD_Mobile/Classify.vue')
  }
];

const router = new VueRouter({
  routes
});

export default router;
