## 过程学习总结

[2022.8.11]

- axios 配置 (create \ 封装等)

[2022.8.13]

- 其他
  1. 单行\多行文字结尾省略
  2. v-html 添加 style (scss -> ::v-deep css -> >>>)
  3. [没完成] :has :not 选择器的具体用法
- vue
  1. 动态路由
  2. transition 动画切换

[2022.8.14]

- css
  1. flex 的高级用法...
     - flex-shrink : 子盒子是否在空间不够的情况下压缩自己
     - flex-wrap : 子盒子是否在空间不够的情况下换行
     - flex 如何解决 flex-wrap 对齐最后一行的问题 [让 CSS flex 布局最后一行列表左对齐的 N 种方法](https://www.zhangxinxu.com/wordpress/2019/08/css-flex-last-align/)
  2. grid 的玩法...
  3. line-height
- vue
  1. computed
  2. v-for key

[2022.8.19]

1. swiper -> vue2 和 vue3 的版本和玩法都不一样
   - vue2 玩 swiper@5 + vue-swiper-awesome@3
   - vue3 再说吧

# vue2_small_project

package manager: pnpm

## Project setup

```
pnpm install
```

### Compiles and hot-reloads for development

```
pnpm serve
```

### Compiles and minifies for production

```
pnpm build
```

## 作业

1. vuex 练习...就是个新闻界面的东西, 联动标签等等
2. XJB 干吧
